variable "tags" {
    description = "Values to tags"
    default = {
        Curso = "Ciências de Dados"
        Disciplina = "Projeto Integrado em Ciências de Dados"
    }
}

variable "resource_group" {
    description = "Projeto Integrado em Ciências de Dados"
    default = "project-data-science"
}

variable "location" {
    description = "Brasil"
    default = "brazilsouth"
}

variable "storageaccount" {
    description = "Storage"
    default = "datalakedatascproj2022"
}

variable "conteiner_datalake" {
description = "Contêiner de dados"
default = "datalake"
}