# Criar uma conta de armazenamento (Storage Account)
resource "azurerm_storage_account" "storageaccount" {
    name = var.storageaccount
    resource_group_name = var.resource_group
    location = var.location
    account_tier = "Standard"
    account_replication_type = "GRS"
    account_kind = "StorageV2"
    is_hns_enabled = "true"
    network_rules {
        default_action = "Allow"
    }
    tags = var.tags
    depends_on = [azurerm_resource_group.project-data-science]
}