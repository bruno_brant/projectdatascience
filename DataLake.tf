# Criar um Cointêiner de dados
resource "azurerm_storage_container" "filesystem_datalake" {
    name = var.conteiner_datalake
    storage_account_name = azurerm_storage_account.storageaccount.name
    container_access_type = "blob"
    depends_on = [azurerm_storage_account.storageaccount]
}