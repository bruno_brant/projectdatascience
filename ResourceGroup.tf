# Criar Resource Group
resource "azurerm_resource_group" "project-data-science" {
    name = var.resource_group
    location = var.location
    tags = var.tags
}